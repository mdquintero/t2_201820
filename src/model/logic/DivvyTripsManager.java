package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Calendar;

import api.IDivvyTripsManager;
import model.data_structures.LinkedList;
import model.vo.VOTrip;

public class DivvyTripsManager implements IDivvyTripsManager {




	private LinkedList<Station> listaEncadenadaStations = new LinkedList<>();
	private LinkedList<VOTrip> listaEncadenadaTrips = new LinkedList<>();



	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub

		try{
			FileReader fr = new FileReader(new File(stationsFile));
			BufferedReader br = new BufferedReader(fr);

			String line = br.readLine();
			//Se avanza de nuevo porque la l�nea anterior no contiene datos
			line = br.readLine();

			while(line!=null)
			{

				System.out.println(line);
				String[] lineArray = line.split(",");

				Station s = new Station(lineArray[0], lineArray[1], lineArray[2], Double.parseDouble(lineArray[3]), Double.parseDouble(lineArray[4]), 
						Integer.parseInt(lineArray[5]), Calendar.getInstance());


				listaEncadenadaStations.add(s);

				line = br.readLine();
			}
			
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	public void loadTrips (String tripsFile) {

		try{
			FileReader fr = new FileReader(new File(tripsFile));
			BufferedReader br = new BufferedReader(fr);

			VOTrip t;
			
			

			String line = br.readLine();
			//Se avanza de nuevo porque la l�nea anterior no contiene datos
			line = br.readLine();

			while(line!=null)
			{

				//System.out.println(line);
				String[] lineArray = line.split(",");

				if(lineArray.length==12){
					t = new VOTrip(Integer.parseInt(lineArray[0]), Calendar.getInstance(), Calendar.getInstance(), Integer.parseInt(lineArray[3]), 
							Integer.parseInt(lineArray[4]), Integer.parseInt(lineArray[5]), lineArray[6], Integer.parseInt(lineArray[7]), lineArray[8], lineArray[9],
							lineArray[10], Integer.parseInt(lineArray[11]));
				}
				else{
					t = new VOTrip(Integer.parseInt(lineArray[0]), Calendar.getInstance(), Calendar.getInstance(), Integer.parseInt(lineArray[3]), 
							Integer.parseInt(lineArray[4]), Integer.parseInt(lineArray[5]), lineArray[6], Integer.parseInt(lineArray[7]), lineArray[8], lineArray[9]);
				}
				listaEncadenadaTrips.add(t);
				
				

				line = br.readLine();
				
				
			}
			
			br.close();

		}
		catch(Exception e){
			e.printStackTrace();
		}


	}

	@Override
	public LinkedList <VOTrip> getTripsOfGender (String gender) {
		LinkedList<VOTrip> listaGeneros = new LinkedList<VOTrip>();

		for(VOTrip recorrido: listaEncadenadaTrips){
			if(recorrido.getGender()!=null){
				if(recorrido.getGender().equals(gender)){
					listaGeneros.add(recorrido);
				}
			}
		}
		return listaGeneros;
	}

	@Override
	public LinkedList <VOTrip> getTripsToStation (int stationID) {
		LinkedList<VOTrip> listaEstaciones = new LinkedList<VOTrip>();

		for(VOTrip recorrido: listaEncadenadaTrips){

			if(recorrido.getIdToStation()==stationID){
				listaEstaciones.add(recorrido);
			}
		}
		return listaEstaciones;
	}	


}
